const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const cors = require("cors");

const searchRoutes = require('./routes/search-routes');
const personRoutes = require('./routes/person-routes');
const accountRoutes = require('./routes/account-routes');
const HttpError = require('./models/http-error');
const constants = require('./constants');

const app = express();

/*
All instances of app.use, are called middleware.
Think of middleware as road exits when you´re driving on a highway.
If your goal destination is colorado, and an exit sign that says "Colorado" appears, you take that exit.

Middleware works in the exact same way.
The code runs from top to bottom, and takes the exit if the client call matches the "sign".
This causes the middleware defined under the executed middleware to never be ran for the specific client call.
*/

// Makes it possible for all ports to talk with one another(built in rule in OS that that is not allowed).
app.use(cors());

// parses the body of all requests(that have one).
app.use(bodyParser.json());

//Sets header for all client calls.
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept, Authorization');
  res.setHeader('Access-Control-Allow-Methods',
    'GET, POST, PATCH, DELETE');

  next();
});

//Send call to searchRoutes if call is to api/search
app.use('/search', searchRoutes);

//Send call to personRoutes if call is to api/person
app.use('/person', personRoutes);

//Send call to accountRoutes if call is to api/account
app.use('/account', accountRoutes);

//Executed if client call wants endpoint which is not defined in api.
app.use((req, res, next) => {
  const error = new HttpError('Kjenner ikke igjen den angitte routen', 404);
  throw error;
});

/*
Built into Express, this middleware is called if an error occurs.
As you can see in most of the error-handling in the backend, return next(error) ends up being handled by this middleware.
The middleware returns a json object to the client, containing the error with companioning error message.
If no such errormessage/errorcode is defined, return error code 500, with message "An unknown error ocurred."
*/
app.use((error, req, res, next) => {
  if (res.headerSent) {
    return next(error);
  }

  res.status(error.code || 500);
  res.json({ message: error.message || 'Det skjedde en uventet feil' });
});

mongoose
  .connect(constants.databaseUrl, { useUnifiedTopology: true })
  .then(() => app.listen(5000))
  .catch(err => console.log(err));