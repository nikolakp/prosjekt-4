const express = require('express');

const searchControllers = require('../controllers/search-controllers');
const auth = require('../authenticate');

const router = express.Router();


/* 
Router to handle POST requests to api/search/{searchSlug}/{page}
If request from client is Post, and searchSlug and int page is given, handled by searchControllers.getSearchResultBySlug
*/
router.post('/:searchSlug/:page', searchControllers.getSearchResultBySlug);

/*
Router to handle all request to api/search which are not picked up by middleware over.
Validates the user, and only if the user is validated, is the request allowed to be processed by middleware defined on line 40.
*/
router.use(auth);

/*
If user is authenticated, and request is GET request targeted at api/search/history, handled by searchControllers.getSearchHistoryForUser
*/
router.get('/history', searchControllers.getSearchHistoryForUser);

router.delete('/history/:hID', searchControllers.deleteSearchHistory);

module.exports = router;
