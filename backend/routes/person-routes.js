const express = require('express');

const personControllers = require('../controllers/person-controllers');
const auth = require('../authenticate');

const router = express.Router();

//Route to handle calls to api/person

/*
If there is a GET call to api/person/{pid}, handled by personControllers.getPerson.
*/
router.get('/:pID', personControllers.getPerson);

/*
If call is not handled by middleware over, user must be authorized to access middleware defined on line 38.
*/
router.use(auth);

// If user is authenticated, and request is POST request, handled by personControllers.createPerson, aka a new person is about to be created with the body of request from client.
router.post('/', personControllers.createPerson);

//Send call to personControllers.updatePerson if call is to /person/{pID}
router.patch('/:pID', personControllers.updatePerson);

router.delete('/:pID', personControllers.deletePerson);

module.exports = router;
