const express = require('express');

const accountControllers = require('../controllers/account-controllers');
const auth = require('../authenticate');

/*
Router called upon by an api call with /account.
*/
const router = express.Router();

//If link is api/account/login, it is handled by accountControllers.loginUser.
router.post('/login', accountControllers.login);

//If link is api/account, it is handled by accountControllers.registerUser.
router.post('/', accountControllers.signup);

/* All api/account calls that are not handled by middleware defined over, is passed onto this one.
The code underneath authenticates the user. If the user is authenticated, and the call is a GET request, handled by accountControllers.getUser.
*/
router.use(auth);

router.get('/', accountControllers.getAccount);

module.exports = router;
