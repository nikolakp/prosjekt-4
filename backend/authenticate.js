const jwt = require('jsonwebtoken');
const HttpError = require('./models/http-error');

const constants = require('./constants');

const authenticateUser = (req, res, next) => {
    try {
        const token = req.headers.authorization.split(' ')[1];
        if (!token) {
            const error = new HttpError('Authentication failed..', 401);
            return next(error);
        }
        const decoded = jwt.verify(token, constants.apiKey);
        req.userData = { userid: decoded.userid, username: decoded.username };
        next();
    } catch (err) {
        const error = new HttpError('Authentication failed..', 401);
        return next(error);
    }
};

module.exports = authenticateUser;