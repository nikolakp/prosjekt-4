const sorting = (data, type, key) => {
    switch (type) {
        case "Years":
            if (sort.key === "Ascending") {
                return data.sort((a, b) => a.sentence.years - b.sentence.years);
            }
            else {
                return data.sort((a, b) => b.sentence.years - a.sentence.years);
            }
        case "City":
            if (key === "Ascending") {
                return data.sort((a, b) => a.location.city < b.location.city ? 1 : -1);
            }
            else {
                return data.sort((a, b) => a.location.city < b.location.city ? -1 : 1);
            };
        default:
            if (key === "Ascending") {
                return data.sort((a, b) => a.name.first < b.name.first ? 1 : -1);
            }
            else {
                return data.sort((a, b) => a.name.first < b.name.first ? -1 : 1);
            };
    };
};

module.exports = sorting;