const mongoose = require('mongoose');

const Schema = mongoose.Schema;

/* Simple model for an account. 
An account must have a username, a password, and be or not be an admin.
*/ 
const accountSchema = new Schema({
    username: { type: String, required: true },
    password: { type: String, required: true },
    admin: { type: Boolean, required: true }
})

module.exports = mongoose.model('Account', accountSchema);