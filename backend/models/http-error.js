/* Simple error extension for HTTP errors.
Customizable error message, and error code.
*/
class HttpError extends Error {
    constructor(message, errorCode) {
        super(message); // Add a "message" property, by calling superMethod Error
        this.code = errorCode; // Add a "code" property
    }
}

module.exports = HttpError;