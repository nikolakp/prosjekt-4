const { ObjectId } = require('mongoose');
const mongoose = require('mongoose');

const Schema = mongoose.Schema;

/* Simple model for a persons search history
A search must have been conducted by a person,
and must be done at a given datetime.
*/
const historySchema = new Schema({
    user: { type: ObjectId, required: true },
    query: { type: String, required: false },
    date: { type: Date, required: true }
})

module.exports = mongoose.model('History', historySchema);