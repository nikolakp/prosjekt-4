const mongoose = require('mongoose');

const Schema = mongoose.Schema;
/* 
Model for a person in the database.
*/
const personSchema = new Schema({
    name: {
        first: { type: String, required: true },
        last: { type: String, required: true },
    },
    gender: { type: String, required: false },
    birthdate: { type: Date, required: true },
    description: { type: String, required: false },
    location: {
        street: { type: String, required: false },
        city: { type: String, required: true }
    },
    sentence: {
        years: { type: Number, required: true },
        incarcerated: { type: Date, required: true },
        reduction: { type: String, required: false },
        misdoing: { type: String, required: true }
    },
    picture: { type: String, required: true },
    related: { type: Array, required: false },
    created: { type: Date, required: false },
    creator: { type: String, required: true }
})

module.exports = mongoose.model('Person', personSchema);