const HttpError = require('../models/http-error');
const Person = require('../models/person');

const constants = require('../constants');

const ObjectId = require('mongoose').Types.ObjectId;
const moment = require('moment')

/*Function to get person by use of id generated on database server.
req is request sent by client, which is the object processed in the function.
res is the response to send back to the client.

parse id in request from client.
Find person by id. If found, return json object to client.

*/
const getPerson = async (req, res, next) => {
    console.log(constants.yellow, 'GET matching the person/:id-endpoint..');
    const personId = req.params.pID;

    if (!personId) {
        return next(new HttpError('Fant ikke ønsket person', 404));
    }

    let person;
    try {
        person = await Person.findById(personId);
    } catch (err) {
        return next(new HttpError('Serverfeil ved henting av person', 500));
    };

    if (!person) {
        return next(new HttpError('Fant ikke bruker(e)', 404));
    };

    console.log('Getting person successful.');
    person = person.toObject({ getters: true })
    res.json(person);
}

/*Function to create a person with body in POST request.
req is request sent by client, which is the object processed in the function.
res is the response to send back to the client.

Client must be authorized to create new person, handled in ./routes/person-routes.js
Create a new user using parameters in request body.
If no errors occur, save the new user on database server, and return json object containing the new user.
*/
const createPerson = async (req, res, next) => {
    console.log(constants.yellow, 'POST matching the person-endpoint..');
    const { name, gender, birthdate, description, location, sentence,
        picture, related, creator } = req.body;

    if (!name || !gender || !birthdate) {
        return next(new HttpError('Mangler informasjon om person', 500));
    }

    const createdPerson = new Person({
        name,
        gender,
        birthdate: moment(birthdate),
        description,
        location,
        sentence,
        picture,
        related,
        creator
    });

    try {
        await createdPerson.save();
    } catch (err) {
        return next(new HttpError('Serverfeil ved oppretting av ny person', 500));
    };

    console.log('Creating person successful.');
    createPerson = createPerson.toObject({ getters: true });
    res.status(201).json(createdPerson);
};

/*Function to update a person by identifying with pID.
req is request sent by client, which is the object processed in the function.
res is the response to send back to the client.
cannot change a person without token, handled in app.js by requiring credentials before middleware is reached.

parse pID from client request.
update the person with the body in POST request. If no errors occur, return the updated person to client.
*/
const updatePerson = async (req, res, next) => {
    console.log(constants.yellow, 'PATCH matching the person/:id-endpoint..');
    const personId = ObjectId(req.params.pID);

    if (!personId) {
        return next(new HttpError('Fant ikke ønsket person', 404));
    }

    let update;
    try {
        update = await Person.updateOne({ _id: personId }, req.body);
    } catch (err) {
        return next(new HttpError('Serverfeil under oppretting av person', 500));
    }

    if (!update) {
        return next(new HttpError('Fant ikke ønsket person(er)', 404));
    }

    let person;
    try {
        person = await Person.findById(personId);
    } catch (err) {
        return next(new HttpError('Serverfeil ved henting av oppdatert person', 500));
    }

    console.log('Updating person successful.');
    person = person.toObject({ getters: true });
    res.status(201).json(person);
};

/*Function to delete a person by identifying with pID.
*/
const deletePerson = async (req, res, next) => {
    console.log(constants.yellow, 'DELETE matching the person/:id-endpoint..');
    const personId = ObjectId(req.params.pID);

    if (!personId) {
        return next(new HttpError('Fant ikke ønsket person', 404));
    }

    let deleted;
    try {
        deleted = await Person.deleteOne({ _id: personId });
    } catch (err) {
        return next(new HttpError('Serverfeil under sletting av person', 500));
    }

    if (!deleted) {
        return next(new HttpError('Fant ikke ønsket person', 404));
    }

    console.log('Deleting person successful.');
    res.status(201).json(deleted);
};

exports.getPerson = getPerson;
exports.createPerson = createPerson;
exports.updatePerson = updatePerson;
exports.deletePerson = deletePerson;