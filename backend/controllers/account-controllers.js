const HttpError = require('../models/http-error');
const Account = require('../models/account');

const constants = require('../constants');

const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

/*Function to handle logging in a user.
req is request sent by client, which is the object processed in the function.
res is the response to send back to the client.

function checks if user exists. If yes, compare req password with database password.
If yes, return the user id, username and token to client.
*/
const login = async (req, res, next) => {
    console.log(constants.pink, 'POST matching the account/login-endpoint..');
    const { username, password } = req.body;

    let account;
    try {
        account = await Account.findOne({ username: username });
    } catch (err) {
        return next(new HttpError('Serverfeil ved innlogging', 500));
    };

    if (!account) {
        return next(new HttpError('Brukeren finnes ikke', 401));
    };

    let passwordMatch;
    try {
        passwordMatch = await bcrypt.compare(password, account.password);
    } catch (err) {
        return next(new HttpError('Serverfeil ved innlogging', 500));
    };

    if (!passwordMatch) {
        return next(new HttpError('Ugyldige kredentialer', 500));
    };

    let token;
    try {
        token = jwt.sign({ userid: account.id, username }, constants.apiKey,
            { expiresIn: '1h' });
    } catch (err) {
        return next(new HttpError('Serverfeil ved innlogging', 500));
    };

    console.log('Login successful.');
    res.status(201).json({ userid: account.id, username, admin: account.admin, token });
}


/*Function to handle registering a new user.
req is request sent by client, which is the object processed in the function.
res is the response to send back to the client.

Parse str username, str password and bool admin.
Try to find existing account.
If existing account exists, return error to client.

If existing account does not exist, hash password sent via HTTPS to API.
If no errors occur, make a new account with the provided acccount details, and save to database.
If no errors occur, return token, userid and name to client.
*/
const signup = async (req, res, next) => {
    console.log(constants.pink, 'POST matching the account/signup-endpoint..');
    const { username, password, admin } = req.body;

    let existingAccount;
    try {
        existingAccount = await Account.findOne({ username });
    } catch (err) {
        return next(new HttpError('Serverfeil ved innlogging', 500));
    };

    if (existingAccount) {
        const error = new HttpError('Brukernavnet er allerede tatt', 422);
        return next(new HttpError('Brukernavnet er allerede tatt', 422));
    };

    let hashedPassword;
    try {
        hashedPassword = await bcrypt.hash(password, 10);
    } catch (err) {
        return next(new HttpError('serverfeil ved oppretting av ny bruker', 500));
    };

    const createdUser = new Account({
        username,
        password: hashedPassword,
        admin,
    });

    try {
        await createdUser.save();
    } catch (err) {
        return next(new HttpError('serverfeil ved oppretting av ny bruker', 500));
    };

    let token;
    try {
        token = jwt.sign({ userid: createdUser.id, username: createdUser.username },
            constants.apiKey, { expiresIn: '1h' });
    } catch (err) {
        return next(new HttpError('serverfeil ved oppretting av ny bruker', 500));
    };

    console.log('Signup successful.');
    res.status(201).json({ userid: createdUser.id, username, admin, token });
};

/*Function to get a user object.
req is request sent by client, which is the object processed in the function.
res is the response to send back to the client.

use token to request userID from database server. If no errors occur, return json object to client.
*/
const getAccount = async (req, res, next) => {
    console.log(constants.pink, 'GET matching the account-endpoint..');
    const token = req.headers.authorization.split(' ')[1];
    const username = req.userData.username;

    let account;
    try {
        account = await Account.findOne({ username });
    } catch (err) {
        return next(new HttpError('serverfeil ved henting av bruker', 500));
    };

    if (!account) {
        return next(new HttpError('Ugyldige kredentialer', 500));
    };

    console.log('Get account successful.');
    res.status(201).json({ token, ...account });
}

exports.login = login;
exports.signup = signup;
exports.getAccount = getAccount;