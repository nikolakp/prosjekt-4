const HttpError = require('../models/http-error');
const Person = require('../models/person');
const History = require('../models/history');

const constants = require('../constants');
const sorting = require('../sorting');

const moment = require('moment')
const jwt = require('jsonwebtoken');
const ObjectId = require('mongoose').Types.ObjectId;

/*Function to search for user by matching slug parameters.
req is request sent by client, which is the object processed in the function.
res is the response to send back to the client.
page is a variable used to figure out which page of users that should be fetched.
In frontend, we have implemented "infinite" scrolling. We´ve achieved this by dynamically fetching the next 20 people
when you reach the bottom of the screen.

Find persons by searching with slug.

Authenticate user with token.

If user is authenticated, get all persons with matching parameters, and log search in search history of user with the authenticated token.
*/
const search = async (req, res, next) => { //Triggered on call to /api/search
    console.log(constants.cyan, 'POST matching the search/:searchword-endpoint..');
    const sort = req.body.filters.sort;
    const searchFilters = req.body.filters.filters;
    const page = parseInt(req.params.page);
    const firstSearchWord = req.params.searchSlug.split("+")[0];
    const secondSearchWord = req.params.searchSlug.split("+")[1] || null;

    const filters = {};
    if (searchFilters.age && searchFilters.age[1]) {
        const birthdateFrom = moment().subtract(searchFilters.age[1], 'years');
        const birthdateTo = moment().subtract(searchFilters.age[0], 'years');

        filters['birthdate'] = { '$gte': birthdateFrom, '$lte': birthdateTo };
    };

    if (searchFilters.gender) {
        filters['gender'] = searchFilters.gender;
    };

    const query = (key, value) => {
        const regexp = {};
        regexp[key] = { "$regex": `^${value}`, "$options": "i" };
        return { ...regexp, ...filters };
    };

    let firstNames = [], lastNames = [], cities = [], misdoings = [];
    try {
        if (secondSearchWord) {
            lastNames = await Person.find({
                ...query("name.first", firstSearchWord),
                ...query("name.last", secondSearchWord)
            });
        }
        else {
            firstNames = await Person.find(query("name.first", firstSearchWord));
            lastNames = await Person.find(query("name.last", secondSearchWord));
        };
        misdoings = await Person.find(query("sentence.misdoing", firstSearchWord));
        cities = await Person.find({ "location.city": firstSearchWord });
    } catch (err) {
        return next(new HttpError('Serverfeil ved søk', 500));
    }

    const searchResults = sorting([...firstNames, ...lastNames, ...cities, ...misdoings],
        sort.type, sort.key);

    saveHistory(req.headers.authorization, req.params.searchSlug);

    const result = searchResults
        .map(r => r.toObject({ getters: true }))
        .reduce((array, person) => {
            if (array.filter(p => p.id === person.id).length) {
                return array;
            }
            return [...array, person];
        }, [])
        .slice(page * 20, (page + 1) * 20)
        .map(r => {
            return {
                name: r.name, birthdate: r.birthdate, picture: r.picture,
                location: { city: r.location.city }, sentence: { years: r.sentence.years }, id: r.id
            }
        });

    console.log(`Search successful with ${result.length} results.`);
    res.json(result);
};

/*Function get search history of a user.
req is request sent by client, which is the object processed in the function.
res is the response to send back to the client.

If user is authenticated, return search history of user.
*/
const getHistoryForAccount = async (req, res, next) => {
    console.log(constants.cyan, 'GET matching the search/history-endpoint..');
    const token = req.headers.authorization.split(' ')[1];

    let userId;
    try {
        const decoded = jwt.verify(token, constants.apiKey);
        userId = ObjectId(decoded.userid);
    } catch (err) {
        return next(new HttpError('Ugyldige kredentialer', 404));
    };

    let histories;
    try {
        histories = await History.find({ user: userId });
    } catch (err) {
        return next(new HttpError('Serverfeil ved henting av logg', 500));
    }

    if (!histories) {
        return next(new HttpError('Fant ingen logg knyttet til brukeren', 404));
    }

    console.log('Getting history for account successful.')
    histories = histories.map(r => r.toObject({ getters: true }));
    res.status(201).json(histories);
}

/*Function for deleting search history of a user.
*/
const deleteHistory = async (req, res, next) => {
    console.log(constants.cyan, 'DELETE matching the search/history/:id-endpoint..');
    const historyId = ObjectId(req.params.hID);

    if (!historyId) {
        return next(new HttpError('Fant ikke ønsket logg', 404));
    }

    let deleted;
    try {
        deleted = await History.deleteOne({ _id: historyId })
    } catch (err) {
        return next(new HttpError('Serverfeil under sletting av logg', 500));
    }

    if (!deleted) {
        const error = new HttpError('Fant ikke logg', 404);
        return next(error);
    }

    console.log('Deleting history successful.')
    res.status(201).json(deleted);
}

// Helper function for main search-function to save search history
// Updates existing history for search word if found
//
const saveHistory = async (auth, searchWord) => {
    if (!auth || auth.split(' ').length <= 1) return;

    const token = auth.split(' ')[1];
    const decodedToken = jwt.verify(token, constants.apiKey);
    const userid = decodedToken.userid;

    let recentHistory;
    try {
        recentHistory = await History.find({
            user: ObjectId(userid),
            date: { $gte: moment().add(-1, "minutes") }
        });
        recentHistory = recentHistory[recentHistory.length - 1]
    } catch (err) {
        return next(new HttpError('Kunne ikke lagre logg', 500));
    };

    if (recentHistory && searchWord.includes(recentHistory.query)) {
        try {
            await History.update({ _id: recentHistory.id }, {
                query: searchWord
            });
        } catch (err) {
            'Could not save history', 500
        };
    }
    else if (!recentHistory || !recentHistory.query.includes(searchWord)) {
        let oldHistory;
        try {
            oldHistory = await History.find({
                user: ObjectId(userid),
                query: searchWord
            });
            oldHistory = oldHistory[oldHistory.length - 1]
        } catch (err) {
            return next(new HttpError('Kunne ikke lagre logg', 500));
        };

        if (oldHistory) {
            try {
                await History.update({ _id: oldHistory.id }, {
                    date: moment()
                })
            } catch (err) {
                return next(new HttpError('Kunne ikke lagre logg', 500));
            };
        }
        else {
            const createdHistory = new History({
                user: ObjectId(userid),
                query: searchWord,
                date: moment(),
            });

            try {
                await createdHistory.save();
            } catch (err) {
                return next(new HttpError('Kunne ikke lagre logg', 500));
            };
        }
    };
};

exports.getSearchResultBySlug = search;
exports.getSearchHistoryForUser = getHistoryForAccount;
exports.deleteSearchHistory = deleteHistory;
