# FengselDB APP

## Team 60, Prosjekt 4

### Innhold

Prosjektet er utviklet av Team 60, og er ment å være et web-basert oppslagsverk som gir bruker 
mulighet til å søke etter fengselsinnsatte, og finne informasjon om hvorfor de er innsatte, 
hvor lenge de har vært innsatt, og annen moro informasjon.
Det er viktig å påpeke at dette <ins>IKKE</ins> skal tas seriøst, og all informasjon gjengitt i applikasjonen er fiktiv.
Gruppen valgte fengselstema fordi dette temaet gir interessante muligheter for å løse oppgaven.

Gjennom hele readme vil _personer_ referere til "fengslede" personer, og _bruker_ referer til bruker av applikasjonen, altså deg.

## Admin-privilegier
For å aksessere full funksjonalitet, bruk følgene adminbruker : 
brukernavn = admin
passord = admin

Ved å benytte seg av administratorbruker, får en tilgang til å oppdatere allerede eksisterende mennesker med ny informasjon.
Dette gjøres ved å søke etter, og trykke på en eksisterende person. Da vil muligheten for å redigere personen vise seg.
Det er mulig for brukere uten adminprivilegier å legge til nye fengselsinsatte.

## Starte applikasjonen

Du må først koble deg på VPN, ettersom port 3000 på VM-en vår var mer sjenert enn vi hadde håpet på.
Dette innebærer at du må laste ned Expo samt. VPN (Cisco AnyConnect) også på mobilen din om du tester der.
Anbefaler helt klart dette for å få best mulig opplevelse.
Kjøre frontend-prosjektet fra root-mappen:
`cd frontend`
`npm install`
`expo start`
Prosjektet skal åpne i nettleseren din, og du kan simulere eller åpne applikasjonen på en fysisk enhet.

### Backend (valgfritt)
For å kjøre API lokalt, start en ny instans av valgfri terminal, og naviger deg inn i mappen "backend".
Deretter, kjør "npm install && npm start". Dette krever fortsatt VPN for databasen sin del.
Deretter må du inn i frontend, og endre URL til din lokale IP, port 5000. Feks `http://192.168.10.200:5000`.
Du vil nå kunne få veldig tydlig opp i terminalen hva APIet gjør under bruk av appen.

## Tech Stack
Gruppen har benyttet seg av den veldokumenterte MERN-stacken.
#### M = [MongoDB](https://www.mongodb.com/)
MongoDB er en svært skalerbar, enkel, objektorientert NoSQL-database. Dataobjekter blir lagret som separate
dokumenter i en "collection", istedenfor å lagre data i kolonner og rader som i en mer tradisjonell databasestruktur.
#### E = [Express.js](https://expressjs.com/)
Express.js er et web-rammeverk for Node.js, med formål å forenkle utviklingen av et node-basert API/backend. Express.js er spesielt enkelt
i bruk ved utvikling av små web-applikasjoner, som denne oppgaven er. Brukt for å utvikle backend.
#### R = [React Native](https://reactnative.dev/docs/getting-started)
React.js er et svært kjent Javascript-bibliotek, som drastisk forenkler og effektiviserer UI-utvikling.
React Native er en utvidelse av React, for bruk til mobilapplikasjoner.
Brukt for å utvikle frontend.
#### N = [Node.js](https://nodejs.org/en/)
Node.js er et cross-platform asynkront runtime-system som gir mulighet for å kjøre javascript-kode på servere, og ikke bare i nettleser.
Brukt for å utvikle backend.

### Kodekonvensjon

Gruppen har valgt å benytte seg av [Googles kodekonvensjon for JavaScript](https://google.github.io/styleguide/jsguide.html).
Det var ikke krav for oppgaven å benytte en kodekonvensjon, men gruppen har likevel valgt å benytte dette for å sikre
god, lesbar kode.

# Krav til teknologi og funksjonalitet

### Applikasjonen skal ha søkegrensesnitt som gir et resultatsett
Gruppen har valgt et veldig enkelt design, hvor bruker blir presentert for et søkefelt ved besøk av siden.
Dette valget ble gjort for å gjøre det så enkelt som mulig for bruker å benytte seg av nettsiden. 
Det er svært vanlig å gå i fellen å overkomplisere brukergrensesnittet ved å presentere all funksjonalitet til bruker med én gang,
som gir dårlige brukeropplevelser. Å forbedre brukeropplevelsen var hovedmotivasjonen bak dropdown-menyer for filtrering og sortering.

### Resultatsettet skal lastes dynamisk (blaing, lasting ved scrolling)
Ved søk, blir de første 20 personene i databasen som oppfyller søkekriteriene presenteres for brukeren. Dersom bruker scrollen ned til enden, 
vil 20 nye brukere bli presentert, helt til det ikke er flere resultater som oppfyller søkekriteriene.
I det brukeren begynner å fylle ut søkefeltet, vil søket kjøre i realtime. Det er derfor ikke nødvendig for bruker å fullføre
queryen før resultater presenteres. Disse valgene ble tatt for å forsterke det dynamiske fokuset vi har på applikasjonen.

### Skal være mulig å få en detaljert visning av hvert objekt i resultatsettet
Når bruker trykker på et søkeresultat, vil mer utfyllende informasjon hentes dynamisk fra backend, og presenteres i strukturert format
som et utvidet kort-element. Formålet med dette var å redusere informasjon presentert til bruker for å ikke overvelde brukere med informasjon når 
bruker leter etter ønsket person, samtidig som mer utfyllende informasjon er svært lett tilgjengelig. 
Gruppen valgte denne løsningen ettersom denne presentasjonsmåten er svært passende for visning av mange objekter, hvor gjerne
bare étt objekt har betydning for bruker. (Et søk er ofte etter én spesifikk person.)

### Skal være støtte for å å interaktivt raffinere søkeresultatet med filterering og sortering
Man kan manipulere søkeresultatet ved hjelp av filtreringsfunksjonene under søkefeltet. 
Sortering og filtrering blir gjort backend, for å unngå problematikk rundt resortering/refiltrering av brukere dersom 20 nye personer blir presentert.

### Logging av søk (ekstra funksjonalitet)
Dersom bruker har registrert en konto og logget inn, har bruker også tilgang til en logg over sine siste søk.
Logikken bak hvordan denne funksjonaliteten er implementert mtp. dynamisk søkefelt, finnes i backend/controllers/search-controllers.

### Brukergenererte data (ekstra funksjonalitet)
Bruker kan registrere en egen konto på nettsiden, som lagrer brukernavn og passord persistent på databaseserveren.
Dersom bruker har registrert seg, har bruker tilgang til søkeloggen sin. Denne lagres persistent på databaseserveren.
Dersom bruker har registrert seg, kan bruker velge å skape en ny fengelsinnsatt(person), som lagres persistent på databaseserveren.
For å oppfylle kravet, har vi implementert 3 former for brukergenererte data.
I sin helhet, bidrar implementasjonen av den overnevnte funksjonaliteten 
til følelsen av at dette er et virkelig, og ikke minst velfungerende produkt.

# Bruk av teknologi

### TypeScript, React Native
React Native applikasjonen er basert på React- og React Native-rammeverkene, skrevet i Typescript.
Vi har benyttet oss av Expo under utviklingen, både i terminalen, og appen på mobiltelefonene våre.

### Bruk expo-cli og skriptet expo init for å komme i gang
Vi har initialisert prosjektet med `expo init -t expo-template-blank-typescript`.

### Database og API installert på virtuell maskin
Databasen er installert på NTNU virtuell maskin, ved hjelp av NginX. Vi installerte mongoDB lokalt på databaseserveren, som er tilknyttet APIet.
APIet kjører på node.js på samme maskinen, og er tilgjengelig på port 3000.

### Hvorfor REST API?
Gruppen valgte å implementere et REST API, istedenfor et API basert på graphQL.
Gruppen vurderte lenge å implementere et graphQL API, da oppgavens kriterier er svært hintende til en graphQL-implementasjon.
Ved bruk av graphQL hadde APIet blitt svært forenklet, ved at vi hadde sluppet å lage separate http-endpoints for kontodata, brukerdata, oppretting av ny bruker osvosv, og heller kunne
samlet dette til ett enkelt endepunkt, med query/mutation-kall. Det er også et kjent problem at REST API-er overfetcher data, som vil si å hente inn mer data enn det som er nødvendig(ikke et problem ved vår implementasjon).

Likevel valgte gruppen å utvikle et REST API. Grunnen til dette, er at vi anser REST APIer som mer skalerbare, samt mer oversiktlige for utenforstående å sette seg inn i.
I tillegg har gruppen en forkjærlighet for REST APIer.

## Koden skal være lettlest og godt strukturert og kommentert
Som nevnt i introduksjonen, har gruppen benyttet seg av [Googles kodekonvensjon for JavaScript](https://google.github.io/styleguide/jsguide.html).
Dette har ført til svært lettlest og strukturert kode.
Koden er også godt kommentert der det er muligheter for misforståelser, eller nødvendig med oppklaring.
I de fleste tilfeller vil god kode ikke trenge kommentarer som forklarer funksjonaliteten, da dette vil være åpenbart for de fleste som har kjennskap til den gitte teknologien.
Likevel har vi valgt å kommentere store deler av koden vår, for å etterlate ingen tvil om hva de forskjellige komponentene gjør.

## Dokumentasjon

### Frontend dependencies, packages
Lister opp de viktigste pakkene/hjelpemidlene brukt i frontend, for ordens skyld.

#### [moment](https://www.npmjs.com/package/moment), javascript-bibliotek, brukes til datohåndtering i logg.

#### [React Redux](https://react-redux.js.org/), brukt for state-håndtering i react.

#### [React Native Elements](https://reactnativeelements.com/docs), bibliotek med UI-elementer til React Native.

#### [node.js](https://nodejs.org/en/), nevnt i Introduksjon.

#### [Redux-thunk](https://github.com/reduxjs/redux-thunk) middleware for Redux, gir mulighet for å returnere funksjoner istedenfor handlinger.

#### [expo](http://expo.io) expo, for å utvikle React Native, og kjøre applikasjonen vår på mobil.

#### [TypeScript](https://www.typescriptlang.org/) superset av javaScript, brukt for strengere typehåndtering, som gjør utvikling mer oversiktlig, og lettere (i lengden).

### Backend dependencies, packages
Lister opp de viktigste pakkene/hjelpemidlene brukt i backend, for ordens skyld.

#### [Express.js](https://expressjs.com/), nevnt i Introduksjon, web-rammeverk for Node.js.

#### [node.js](https://nodejs.org/en/), nevnt i Introduksjon.

#### [nodemon](https://www.npmjs.com/package/nodemon), devtool for node.js, brukt for å slippe å måtte restarte lokal backend-server manuelt for å reflektere oppdateringer i kode.

#### [NginX](https://www.nginx.com/), brukt for å aksessere databasen som er installert på virtuell maskin.

#### [bcrypt.js](https://www.npmjs.com/package/bcryptjs), brukes til å lagre passord i backend. Bruker hashing + salting for sikker lagring, og for å aldri måtte behandle klartekst-passord.

#### [body-parser](https://www.npmjs.com/package/body-parser), parsing middleware, brukes for å modularisere innkommende request bodies i APIet.

#### [cors](https://www.npmjs.com/package/cors), brukes til å gjøre cross-origin resource sharing enklere.

#### [jsonwebtoken](https://www.npmjs.com/package/jsonwebtoken), brukes til å gjøre overføring av JSON-objekter mellom instanser sikrere.

#### [mongoose](https://mongoosejs.com/) brukes til å forenkle backend objekt-modellering ved bruk av mongoDB
