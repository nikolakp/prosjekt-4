export const selectSearchResultsForSlugPageMeta = (state: any) => {
    let key = '';
    if (state.search.searchWord) {
        key = `${state.search.searchWord.split(' ').join('+')}_${state.search.paging}`;
    }

    return state.search.meta[key] || {
        loading: false,
        error: false,
        loadedAt: null,
    };
};

export const selectSearchResultsForSlugPage = (state: any) => {
    if (state.search.searchWord === "") return [];
    let i, result: any = [];
    for (i = 0; i <= state.search.paging; i++) {
        const key = `${state.search.searchWord.split(' ').join('+')}_${i}`
        result = [...result, ...state.search.entities[key] || []];
    }
    return result;
};

export const selectHistory = (state: any) => {
    if (!state.account.me) return {};
    return state.search.histories.entities[state.account.me.username];
};

export const selectHistoryMeta = (state: any) => {
    if (!state.account.me) return {
        loading: true,
        error: false,
        loadedAt: null,
    };
    return state.search.histories.meta[state.account.me.username] || {
        loading: true,
        error: false,
        loadedAt: null,
    };
};

export const selectSearchWord = (state: any) =>
    state.search.searchWord;

export const selectPage = (state: any) =>
    state.search.paging;

