import moment from "moment";

interface state {
    searchWord: string,
    paging: number,
    entities: { [key: string]: any };
    histories: { entities: { [key: string]: any }, meta: { [key: string]: any } };
    meta: { [key: string]: any };
}


const initialState: state = {
    searchWord: '',
    paging: 0,
    entities: {},
    histories: { entities: {}, meta: {} },
    meta: {},
};

export default (state = initialState, action: any) => {
    switch (action.type) {
        case "SEARCH_START":
            return {
                ...state,
                meta: {
                    ...state.meta,
                    [action.payload.key]: {
                        loading: true,
                        error: false,
                        loadedAt: null,
                        filters: action.payload.filters
                    },
                },
            };
        case "SEARCH_SUCCESS":
            return {
                ...state,
                searchWord: action.payload.slug,
                entities: {
                    ...state.entities,
                    [action.payload.key]: action.payload.searchResult,
                },
                meta: {
                    ...state.meta,
                    [action.payload.key]: {
                        loading: false,
                        error: false,
                        loadedAt: moment().unix(),
                        filters: action.payload.filters
                    },
                },
            }
        case "SEARCH_ERROR":
            return {
                ...state,
                meta: {
                    ...state.meta,
                    [action.payload.key]: {
                        loading: false,
                        error: true,
                        loadedAt: moment().unix(),
                        filters: action.payload.filters
                    },
                },
            };
        case "GET_HISTORY_START":
            return {
                ...state,
                histories: {
                    ...state.histories,
                    meta: {
                        ...state.histories.meta,
                        [action.payload.user.username]: {
                            loading: true,
                            error: false,
                            loadedAt: null,
                        },
                    },
                },
            };
        case "GET_HISTORY_SUCCESS":
            return {
                ...state,
                histories: {
                    ...state.histories,
                    entities: {
                        [action.payload.user.username]: action.payload.history,
                    },
                    meta: {
                        ...state.meta,
                        [action.payload.user.username]: {
                            loading: false,
                            error: false,
                            loadedAt: moment().unix(),
                        }
                    },
                },
            };
        case "GET_HISTORY_ERROR":
            return {
                ...state,
                histories: {
                    ...state.histories,
                    meta: {
                        ...state.histories.meta,
                        [action.payload.user.username]: {
                            loading: false,
                            error: true,
                            loadedAt: moment().unix(),
                        },
                    },
                },
            };
        case "DELETE_HISTORY_START":
            return {
                ...state,
                histories: {
                    ...state.histories,
                    meta: {
                        ...state.histories.meta,
                        [action.payload.user.username]: {
                            loading: true,
                            error: false,
                            loadedAt: null,
                        },
                    },
                },
            };
        case "DELETE_HISTORY_SUCCESS":
            return {
                ...state,
                histories: {
                    ...state.histories,
                    entities: {
                        [action.payload.user.username]:
                            state.histories.entities[action.payload.user.username]
                                .filter((h: any) => h.id !== action.payload.historyid),
                    },
                    meta: {
                        ...state.meta,
                        [action.payload.user.username]: {
                            loading: false,
                            error: false,
                            loadedAt: moment().unix(),
                        }
                    },
                },
            };
        case "DELETE_HISTORY_ERROR":
            return {
                ...state,
                histories: {
                    ...state.histories,
                    meta: {
                        ...state.histories.meta,
                        [action.payload.user.username]: {
                            loading: false,
                            error: true,
                            loadedAt: moment().unix(),
                        },
                    },
                },
            };
        case "SET_SEARCHWORD":
            return {
                ...state,
                searchWord: action.payload.slug,
            };
        case "SET_PAGING":
            return {
                ...state,
                paging: action.payload.page,
            };
        default:
            return state;
    }
};