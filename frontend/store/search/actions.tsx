const apiUrl = "http://it2810-60.idi.ntnu.no:5000";

const foundInBuffer = (getState: any, key: any, filters: any) => {
    const bufferedMeta = getState().search.meta[key];
    return bufferedMeta && !bufferedMeta.error && bufferedMeta.filters === filters;
}

export const searchBySlug = (slug: String, filters: any, page: Number) => (dispatch: any, getState: any) => {
    const searchWord = page ? getState().search.searchWord.split(' ').join('+') : slug.split(' ').join('+');
    const key = `${searchWord}_${page}`;

    if (foundInBuffer(getState, key, filters) || searchWord === "") return dispatch({
        type: "SET_SEARCHWORD",
        payload: {
            slug: searchWord
        },
    });

    dispatch({
        type: "SEARCH_START",
        payload: {
            filters,
            key
        },
    });

    const headers = new Headers();
    headers.append("Content-Type", "application/json");

    if (getState().account.me) {
        headers.append("Authorization", `Bearer ${getState().account.me.token}`)
    }

    return fetch(`${apiUrl}/search/${searchWord.split(' ').join('+')}/${page}`, {
        method: "POST", headers,
        body: JSON.stringify({ filters })
    })
        .then(res => res.json())
        .then(response => {
            dispatch({
                type: "SEARCH_SUCCESS",
                payload: {
                    searchResult: response,
                    filters,
                    key,
                    slug: searchWord
                },
            });
        })
        .catch(error => {
            console.error(error);
            dispatch({
                type: "SEARCH_ERROR",
                payload: {
                    error,
                    filters,
                    key
                },
            });
        });
}

export const getUserSearchHistory = () => (dispatch: any, getState: any) => {
    if (!getState().account.me) return;

    dispatch({
        type: "GET_HISTORY_START",
        payload: {
            user: getState().account.me
        },
    });

    const headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Authorization", `Bearer ${getState().account.me.token}`)

    return fetch(`${apiUrl}/search/history`, {
        method: "GET", headers,
    })
        .then(res => res.json())
        .then(response => {
            dispatch({
                type: "GET_HISTORY_SUCCESS",
                payload: {
                    history: response,
                    user: getState().account.me
                },
            });
        })
        .catch(error => {
            console.error(error);
            dispatch({
                type: "GET_HISTORY_ERROR",
                payload: {
                    error,
                    user: getState().account.me
                },
            });
        });
}

export const deleteSearchHistory = (historyid: string) => (dispatch: any, getState: any) => {
    if (!getState().account.me) return;

    dispatch({
        type: "DELETE_HISTORY_START",
        payload: {
            user: getState().account.me
        },
    });

    const headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Authorization", `Bearer ${getState().account.me.token}`)

    return fetch(`${apiUrl}/search/history/${historyid}`, {
        method: "DELETE", headers,
    })
        .then(res => res.ok ? res.json() : null)
        .then(() => {
            dispatch({
                type: "DELETE_HISTORY_SUCCESS",
                payload: {
                    historyid: historyid,
                    user: getState().account.me
                },
            });
        })
        .catch(error => {
            console.error(error);
            dispatch({
                type: "DELETE_HISTORY_ERROR",
                payload: {
                    error,
                    user: getState().account.me
                },
            });
        });
}

export const setPage = (page: Number) => (dispatch: any) => {
    return dispatch({
        type: "SET_PAGING",
        payload: {
            page
        },
    });

}