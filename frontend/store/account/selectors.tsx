export const selectAccountMeta = (state: any) =>
    state.account.meta;

export const selectAccount = (state: any) =>
    state.account.me;