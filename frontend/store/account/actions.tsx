import { Actions } from 'react-native-router-flux';

const apiUrl = "http://it2810-60.idi.ntnu.no:5000";

export const loginAccount = (username: string, password: string) => (dispatch: any) => {
    dispatch({
        type: "LOGIN_START",
        payload: {
            username,
        },
    });

    return fetch(`${apiUrl}/account/login`, {
        method: "POST", headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ username, password })
    })
        .then(res => res.json())
        .then(response => {
            if (!response.message) {
                dispatch({
                    type: "LOGIN_SUCCESS",
                    payload: {
                        response,
                    },
                });
                setTimeout(Actions.home, 1000);
            } else {
                dispatch({
                    type: "LOGIN_ERROR",
                    payload: {
                        error: response.message,
                        username,
                    },
                });
            };
        })
        .catch(error => {
            dispatch({
                type: "LOGIN_ERROR",
                payload: {
                    error,
                    username,
                },
            });
        });
}

export const getAccount = (token: string) => (dispatch: any) => {
    dispatch({
        type: "GET_ACCOUNT_START",
        payload: {
            token,
        },
    });

    return fetch(`${apiUrl}/account`, {
        method: "GET", headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${token}`
        },
    })
        .then(res => res.json())
        .then(response => {
            dispatch({
                type: "GET_ACCOUNT_SUCCESS",
                payload: {
                    response,
                },
            });
        })
        .catch(error => {
            console.error(error);
            dispatch({
                type: "GET_ACCOUNT_ERROR",
                payload: {
                    error,
                    token,
                },
            });
        });
}


export const registerAccount = (username: string, password: string) => (dispatch: any) => {
    dispatch({
        type: "REGISTER_START",
        payload: {
            username,
        },
    });

    return fetch(`${apiUrl}/account`, {
        method: "POST", headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ username, password, admin: false })
    })
        .then(res => res.json())
        .then(response => {
            if (!response.message) {
                dispatch({
                    type: "REGISTER_SUCCESS",
                    payload: {
                        response,
                    },
                });
                setTimeout(Actions.home, 1000);
            } else {
                dispatch({
                    type: "REGISTER_ERROR",
                    payload: {
                        error: response.message,
                        username,
                    },
                });
            };
        })
        .catch(error => {
            console.error(error);
            dispatch({
                type: "REGISTER_ERROR",
                payload: {
                    error,
                    username,
                },
            });
        });
}

export const logoutAccount = () => (dispatch: any) => {
    return dispatch({
        type: "LOGOUT",
        payload: null,
    });
}
