import moment from "moment";

const initialState = {
    me: null,
    meta: {
        loading: false,
        error: false,
        loadedAt: null,
        ids: [],
    },
};

export default (state = initialState, action: any) => {
    switch (action.type) {
        case "LOGIN_START":
            return {
                ...state,
                me: null,
                meta: {
                    loading: true,
                    error: false,
                    loadedAt: null,
                },
            };
        case "LOGIN_SUCCESS":
            return {
                ...state,
                me: {
                    token: action.payload.response.token,
                    userid: action.payload.response.userid,
                    username: action.payload.response.username,
                    admin: action.payload.response.admin
                },
                meta: {
                    loading: true,
                    error: false,
                    loadedAt: moment().unix(),
                },
            }
        case "LOGIN_ERROR":
            return {
                ...state,
                me: null,
                meta: {
                    loading: false,
                    error: action.payload.error,
                    loadedAt: moment().unix(),
                },
            };
        case "GET_ACCOUNT_START":
            return {
                ...state,
                me: null,
                meta: {
                    loading: true,
                    error: false,
                    loadedAt: null,
                },
            };
        case "GET_ACCOUNT_SUCCESS":
            return {
                ...state,
                me: {
                    token: action.payload.response.token,
                    userid: action.payload.response.userid,
                    username: action.payload.response.username,
                    admin: action.payload.response.admin
                },
                meta: {
                    loading: false,
                    error: false,
                    loadedAt: moment().unix(),
                },
            };
        case "GET_ACCOUNT_ERROR":
            return {
                ...state,
                me: null,
                meta: {
                    loading: false,
                    error: true,
                    loadedAt: moment().unix(),
                },
            };
        case "REGISTER_START":
            return {
                ...state,
                me: null,
                meta: {
                    loading: true,
                    error: false,
                    loadedAt: null,
                },
            };
        case "REGISTER_SUCCESS":
            return {
                ...state,
                me: {
                    token: action.payload.response.token,
                    userid: action.payload.response.userid,
                    username: action.payload.response.username
                },
                meta: {
                    loading: false,
                    error: false,
                    loadedAt: moment().unix(),
                },
            };
        case "REGISTER_ERROR":
            return {
                ...state,
                me: null,
                meta: {
                    loading: false,
                    error: action.payload.error,
                    loadedAt: moment().unix(),
                },
            };
        case "LOGOUT":
            return {
                ...state,
                me: null,
                meta: {
                    loading: false,
                    error: false,
                    loadedAt: null,
                },
            };
        default:
            return state;
    }
};