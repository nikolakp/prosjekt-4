const apiUrl = "http://it2810-60.idi.ntnu.no:5000";

const foundInBuffer = (getState: any, personid: any) => {
    const bufferedMeta = getState().person.meta[personid];
    return bufferedMeta && !bufferedMeta.error;
}

export const getPerson = (personid: any) => (dispatch: any, getState: any) => {
    if (foundInBuffer(getState, personid)) return;

    dispatch({
        type: "GET_PERSON_START",
        payload: {
            id: personid,
        },
    });

    return fetch(`${apiUrl}/person/${personid}`, {
        method: "GET", headers: { "Content-Type": "application/json" }
    })
        .then(res => res.json())
        .then(response => {
            dispatch({
                type: "GET_PERSON_SUCCESS",
                payload: {
                    person: response
                },
            });
        })
        .catch(error => {
            console.error(error);
            dispatch({
                type: "GET_PERSON_ERROR",
                payload: {
                    error,
                    id: personid,
                },
            });
        });
}

export const addPerson = (person: any) => (dispatch: any, getState: any) => {
    dispatch({
        type: "ADD_PERSON_START",
        payload: {
            person,
        },
    });

    const headers = new Headers();
    headers.append("Content-Type", "application/json");

    if (getState().account.me) {
        headers.append("Authorization", `Bearer ${getState().account.me.token}`)
    }

    return fetch(`${apiUrl}/person`, {
        method: "POST", headers,
        body: JSON.stringify(person)
    })
        .then(res => res.json())
        .then(response => {
            dispatch({
                type: "ADD_PERSON_SUCCESS",
                payload: {
                    person: response
                },
            });
        })
        .catch(error => {
            console.error(error);
            dispatch({
                type: "ADD_PERSON_ERROR",
                payload: {
                    error,
                    person,
                },
            });
        });
}

export const updatePerson = (person: any) => (dispatch: any, getState: any) => {
    dispatch({
        type: "UPDATE_PERSON_START",
        payload: {
            person,
        },
    });

    const headers = new Headers();
    headers.append("Content-Type", "application/json");

    if (getState().account.me) {
        headers.append("Authorization", `Bearer ${getState().account.me.token}`)
    }

    return fetch(`${apiUrl}/person/${person.id}`, {
        method: "PATCH", headers,
        body: JSON.stringify(person)
    })
        .then(res => res.json())
        .then(response => {
            dispatch({
                type: "UPDATE_PERSON_SUCCESS",
                payload: {
                    person: response
                },
            });
        })
        .catch(error => {
            console.error(error);
            dispatch({
                type: "UPDATE_PERSON_ERROR",
                payload: {
                    error,
                    person,
                },
            });
        });
}