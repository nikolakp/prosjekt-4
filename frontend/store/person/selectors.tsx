export const selectPersonMeta = (state: any, id: any) =>
    state.person.meta[id] || {
        loading: true,
        error: false,
        loadedAt: null
    };

export const selectPerson = (state: any, id: any) =>
    state.person.entities[id] || {};
