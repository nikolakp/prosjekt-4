import moment from "moment";

const initialState = {
    entities: {},
    meta: {},
};

export default (state = initialState, action: any) => {
    switch (action.type) {
        case "GET_PERSON_START":
            return {
                ...state,
                meta: {
                    ...state.meta,
                    [action.payload.id]: {
                        loading: true,
                        error: false,
                        loadedAt: null,
                    },
                },
            };
        case "GET_PERSON_SUCCESS":
            return {
                ...state,
                entities: {
                    ...state.entities,
                    [action.payload.person.id]: action.payload.person
                },
                meta: {
                    ...state.meta,
                    [action.payload.person.id]: {
                        loading: false,
                        error: false,
                        loadedAt: moment().unix(),
                    },
                },
            };
        case "GET_PERSON_ERROR":
            return {
                ...state,
                meta: {
                    ...state.meta,
                    [action.payload.id]: {
                        loading: false,
                        error: true,
                        loadedAt: moment().unix(),
                    },
                },
            };
        case "ADD_PERSON_START":
            return {
                ...state,
                meta: {
                    ...state.meta,
                    [action.payload.id]: {
                        loading: true,
                        error: false,
                        loadedAt: null,
                    },
                },
            };
        case "ADD_PERSON_SUCCESS":
            return {
                ...state,
                entities: {
                    ...state.entities,
                    [action.payload.person.id]: action.payload.person
                },
                meta: {
                    ...state.meta,
                    [action.payload.person.id]: {
                        loading: false,
                        error: false,
                        loadedAt: moment().unix(),
                    },
                },
            };
        case "ADD_PERSON_ERROR":
            return {
                ...state,
                meta: {
                    ...state.meta,
                    [action.payload.id]: {
                        loading: false,
                        error: true,
                        loadedAt: moment().unix(),
                    },
                },
            };
        case "UPDATE_PERSON_START":
            return {
                ...state,
                meta: {
                    ...state.meta,
                    [action.payload.person.id]: {
                        loading: true,
                        error: false,
                        loadedAt: null,
                    },
                },
            };
        case "UPDATE_PERSON_SUCCESS":
            return {
                ...state,
                entities: {
                    ...state.entities,
                    [action.payload.person.id]: action.payload.person
                },
                meta: {
                    ...state.meta,
                    [action.payload.person.id]: {
                        loading: false,
                        error: false,
                        loadedAt: moment().unix(),
                    },
                },
            };
        case "UPDATE_PERSON_ERROR":
            return {
                ...state,
                meta: {
                    ...state.meta,
                    [action.payload.person.id]: {
                        loading: false,
                        error: true,
                        loadedAt: moment().unix(),
                    },
                },
            };
        default:
            return state;
    }
};
