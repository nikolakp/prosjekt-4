import { combineReducers } from 'redux';
import accountReducer from "./account/reducer";
import personReducer from "./person/reducer";
import searchReducer from "./search/reducer";


export default combineReducers({
    account: accountReducer, person: personReducer, search: searchReducer
});