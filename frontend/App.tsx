import React from 'react';
import { View } from 'react-native';
import { Router, Scene } from 'react-native-router-flux';
import { Provider } from 'react-redux';
import About from './components/About';
import Header from './components/Header';
import History from './components/History';
import Login from './components/Login';
import Menu from './components/Menu';
import PersonForm from './components/PersonForm';
import Results from './components/Results';
import Search from './components/Search';
import store from "./store/store";


export default function App() {
  const Home = () => {
    return (
      <View >
        <Header />
        <Search />
        <Results />
      </View>
    )
  };

  return (
    <Provider store={store}>
      <Router >
        <Scene key="root" hideNavBar >
          <Scene key="home" component={Home} initial />
          <Scene key="login" component={Login} />
          <Scene key="history" component={History} />
          <Scene key="menu" component={Menu} />
          <Scene key="personForm" component={PersonForm} />
          <Scene key="about" component={About} />
        </Scene>
      </Router>
    </Provider>
  );
};