import React, { useEffect } from "react";
import {
  Alert,
  Modal,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ActivityIndicator
} from "react-native";
import { connect } from 'react-redux';
import { faMapMarkerAlt, faTimes } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { Avatar } from 'react-native-elements';
import { getPerson } from '../store/person/actions';
import { selectPerson, selectPersonMeta } from '../store/person/selectors';
import { selectAccount } from '../store/account/selectors';
import moment from 'moment';
import { faBlackTie } from "@fortawesome/free-brands-svg-icons";

const PersonPopup = (props: any) => {
  if (!props.personId) return null;

  useEffect(() => {
    props.getPerson(props.personId);
  }, [props.personId]);

  if (props.meta.loading) return (
    <View style={styles.centeredView}>
      <View style={styles.modalView}>
        <ActivityIndicator size="large" color="#003f5c" />
      </View>
    </View>
  );

  //set the person info from the prop.
  const { name, birthdate, location, sentence, picture, description, gender } = props.person;

  return (
    <View style={styles.centeredView}>
      <Modal
        animationType="slide"
        transparent
        visible
      >
        <View style={styles.modalView}>
          <TouchableOpacity onPress={props.onClose} style={styles.top}>
            <FontAwesomeIcon icon={faTimes} size={25} color="#dbdbdb" />
          </TouchableOpacity>
          {/* HEADER */}
          <View style={styles.header}>
            <Text style={styles.titleText}>{name.first} {name.last}</Text>
            <Text style={styles.titleText}>{gender.toUpperCase()}, {moment(birthdate).format('DD.MM.YYYY')}</Text>
          </View>

          {/* IMAGE */}
          <View style={styles.imageView}>
            <Avatar rounded
              size="xlarge"
              source={{ uri: picture, }}
            />
          </View>

          {/* SENTENCE */}
          <View style={styles.blockSentence}>
            <Text style={{ color: "white" }}>{sentence.years} for {sentence.misdoing}</Text>
          </View>
          
          {/* LOCATION */}
          <View style={styles.blockLocation}>
            <View><FontAwesomeIcon icon={faMapMarkerAlt} size={25} color="#404040" /></View>
            <Text style={{ marginRight: 50, marginLeft: 10, }}>{location.street} - {location.city}</Text>
          </View>

          {/* DESCRIPTION */}
          <View style={styles.blockDescription}>
            <Text>{description}</Text>
          </View>
        </View>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  blockSentence: {
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#3b3b3b",
    height: 50,
    width: 300,
  },
  blockLocation: {
    marginTop: 15,
    paddingLeft: 20,
    flexDirection: "row",
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#737373",
    height: 60,
    width: 300,
  },
  blockDescription: {
    marginVertical: 15,
    paddingVertical: 15,
    paddingHorizontal: 20,
    flexDirection: "row",
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "flex-start",
    backgroundColor: "#737373",
    height: "auto",
    width: 300,
  },
  header: {
    justifyContent: "center",
    // backgroundColor: "steelblue",
    paddingBottom: 20,
    margin: -20
  },
  imageView: {
    marginBottom: 20,
    marginTop: 20,
  },
  titleText: {
    fontWeight: "bold",
    fontSize: 20,
    // color: "#66D3FA",
    color: "#dbdbdb",
  },
  top: {
    marginLeft: 'auto'
  },
  modalView: {
    marginTop: 'auto',
    marginBottom: 'auto',
    margin: 20,
    backgroundColor: "#616161",
    opacity: 0.97, //May remove opacity if it is disturbing
    borderRadius: 20,
    padding: 15,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 10,
      height: 10,
    },
    shadowOpacity: 0.25,
    shadowRadius: 50,
    elevation: 5
  },
});


export default connect(
  (state: any, ownProps: any) => {
    if (!ownProps.personId) {
      return {};
    }
    const person = selectPerson(state, ownProps.personId);
    const meta = selectPersonMeta(state, ownProps.personId);
    const me = selectAccount(state);
    return { person, meta, me };
  }, { getPerson })(PersonPopup);