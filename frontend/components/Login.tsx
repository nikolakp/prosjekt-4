import React, { useEffect, useState } from 'react';
import {
    StyleSheet, Text, TextInput,
    TouchableOpacity, View, Vibration
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { loginAccount, logoutAccount, registerAccount } from "../store/account/actions";
import { selectAccount, selectAccountMeta } from "../store/account/selectors";
import createOneButtonAlert from "./ErrorPopup";


const Login = (props: any) => {
    const [register, setRegister] = useState(false);
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [error, setError] = useState("");
    const [checkPassword, setCheckPassword] = useState("");

    useEffect(()=> {
        if (props.meta.error) {
            createOneButtonAlert("Error", props.meta.error);
            setError("");
            return
        };
    },[props.meta.error]);

    const handleLoginButton = () => {
        Vibration.vibrate();
        if (!username || !password) {
            setError("Skriv inn brukernavn og passord");
            return
        };
        //If the login-button is clicked, it will tell TopBar that user is now logged in.
        props.loginAccount(username, password);
        setError("");
    };

    const handleRegisterButton = () => {
        Vibration.vibrate();
        if (!username || !password || !checkPassword) {
            setError("Skriv inn ønsket brukernavn og passord");
            return
        }

        if (password !== checkPassword) {
            createOneButtonAlert("Error", "Passwords must match")
            setError("");
            return
        }
        //If the register-button is clicked, it will tell TopBar that user has registered.
        if (password === checkPassword && password.length > 3) {
            props.registerAccount(username, password);
            setError("");
        }
        else {
            createOneButtonAlert("Error", "Password must be longer than 3 characters");
            setError("");
        };
    }

    if (props.account) {
        return (
            <View style={styles.container}>
                <Text style={styles.logo}>
                    {props.account.username}
                </Text>
                {/* LOGG UT */}
                <TouchableOpacity style={styles.loginButton} onPress={props.logoutAccount}>
                    <Text style={styles.loginText} >LOGG UT</Text>
                </TouchableOpacity>
                {/* Gå til hjem */}
                <TouchableOpacity onPress={Actions.home}>
                    <Text style={styles.bottomText} >Tilbake</Text>
                </TouchableOpacity>
            </View>
        )
    };

    if (register) {
        return (
            <View style={styles.container}>
                <Text style={styles.logo}>
                    Registrer deg!
                </Text>
                <Text style={styles.errorText}>
                    {error}
                </Text>

                {/* Brukernavn */}
                <View style={styles.inputView} >
                    <TextInput
                    value={username}
                        style={styles.inputText}
                        placeholder="Brukernavn"
                        onChangeText={setUsername} />
                </View>

                {/* Passord */}
                <View style={styles.inputView} >
                    <TextInput
                    value={password}
                        style={styles.inputText}
                        placeholder="Passord"
                        onChangeText={setPassword}
                        secureTextEntry />
                </View>

                {/* Gjenta Passord */}
                <View style={styles.inputView} >
                    <TextInput
                        value={checkPassword}
                        style={styles.inputText}
                        placeholder="Gjenta passord"
                        onChangeText={setCheckPassword}
                        secureTextEntry />
                </View>

                {/* REGISTRER */}
                <TouchableOpacity style={styles.loginButton} onPress={handleRegisterButton}>
                    <Text style={styles.loginText} >REGISTRER</Text>
                </TouchableOpacity>

                {/*<Text>
                    {props.meta.error || error || ''}
                </Text>*/}

                <View style={styles.bottom}>
                    {/* Bytt til logg inn */}
                    <TouchableOpacity onPress={() => {
                        setRegister(false);
                        setError("");
                        setUsername("");
                        setPassword("");
                        setCheckPassword("");
                    }}>
                        <Text style={styles.bottomText}>Logg inn</Text>
                    </TouchableOpacity>
                    {/* Gå til hjem */}
                    <TouchableOpacity onPress={Actions.home}>
                        <Text style={styles.bottomText}>Tilbake</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    };

    return (
        <View style={styles.container}>
            <Text style={styles.logo}>
                Logg inn
            </Text>
            <Text style={styles.errorText}>
                {error}
            </Text>

            {/* Brukernavn */}
            <View style={styles.inputView} >
                <TextInput
                    value={username}
                    style={styles.inputText}
                    placeholder="Brukernavn"
                    onChangeText={setUsername} />
            </View>

            {/* Passord */}
            <View style={styles.inputView} >
                <TextInput
                    value={password}
                    style={styles.inputText}
                    placeholder="Passord"
                    onChangeText={setPassword}
                    secureTextEntry />
            </View>


            {/* LOGG INN */}
            <TouchableOpacity style={styles.loginButton} onPress={handleLoginButton}>
                <Text style={styles.loginText}>LOGG INN</Text>
            </TouchableOpacity>

            <View style={styles.bottom}>
                {/* Bytt til registrer */}
                <TouchableOpacity onPress={() => {
                    setRegister(true);
                    setUsername("");
                    setPassword("");
                    setError("");
                }}>
                    <Text style={styles.bottomText}>Registrer</Text>
                </TouchableOpacity>
                {/* Gå til hjem */}
                <TouchableOpacity onPress={Actions.home}>
                    <Text style={styles.bottomText}>Tilbake</Text>
                </TouchableOpacity>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#003f5c',
        alignItems: 'center',
        justifyContent: 'center',
    },
    logo: {
        fontWeight: "bold",
        fontSize: 55,
        color: "#66D3FA",
        marginBottom: 20
    },
    bottom: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-around",
    },
    inputView: {
        width: "80%",
        backgroundColor: "#C7E9FF",
        borderRadius: 25,
        height: 50,
        marginBottom: 20,
        justifyContent: "center",
        padding: 20
    },
    inputText: {
        height: 50,
        color: "#003f5c",
    },
    bottomText: {
        color: "#A5BCCB",
        fontSize: 15,
        margin: 20,
    },
    loginText: {
        color: "white",
    },
    errorText: {
        color: "red",
        marginBottom: 50,
    },
    loginButton: {
        width: "80%",
        backgroundColor: "#009AFF",
        borderRadius: 25,
        height: 50,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 20,
        marginBottom: 30
    },
});


export default connect((state: any) => {
    return { account: selectAccount(state), meta: selectAccountMeta(state) };
}, { loginAccount, registerAccount, logoutAccount })(Login);