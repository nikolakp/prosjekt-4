import React, { useState } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { Actions } from 'react-native-router-flux';

const Menu = () => {
    return (
        <View style={styles.menu}>
            <Text style={styles.logo}>
                FengselDB
            </Text>

            <TouchableOpacity onPress={Actions.history}>
                <Text style={styles.link}>HISTORIKK</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={Actions.personForm}>
                <Text style={styles.link}>LEGG TIL</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={Actions.about}>
                <Text style={styles.link}>HJELP</Text>
            </TouchableOpacity>

            <View style={styles.bottom}>
                <TouchableOpacity onPress={Actions.login}>
                    <Text style={styles.bottomText}>Min profil</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={Actions.home}>
                    <Text style={styles.bottomText}>Fortsett</Text>
                </TouchableOpacity>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    menu: {
        flex: 1,
        backgroundColor: '#003f5c',
        alignItems: 'center',
        justifyContent: 'flex-start',
    },
    logo: {
        fontWeight: "bold",
        fontSize: 60,
        color: "#66D3FA",
        marginTop: 100,
        marginBottom: 80
    },
    link: {
        color: "#A5BCCB",
        fontSize: 35,
        margin: 25,
    },
    bottomText: {
        color: "#A5BCCB",
        fontSize: 15,
        margin: 30,
    },
    bottom: {
        position: 'absolute',
        bottom: 30,
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-around",
    }
});

export default Menu;