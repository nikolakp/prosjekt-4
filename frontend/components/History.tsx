import React, { useEffect } from 'react';
import { StyleSheet, Text, View, FlatList } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { selectAccount } from "../store/account/selectors";
import { deleteSearchHistory, getUserSearchHistory } from "../store/search/actions";
import { selectHistory, selectHistoryMeta } from "../store/search/selectors";
import Header from './Header';


const History = (props: any) => {
    useEffect(() => {
        props.getUserSearchHistory();
        !props.account && Actions.login();
    }, []);

    if (!props.account) {
        return (
            <View style={styles.history}>
                <Header />
                <View style={styles.content}>
                    <Text style={styles.text}>Logg inn for å se historikk</Text>
                </View>
            </View>
        )
    };

    return (
        <View style={styles.history}>
            <Header />
            <View style={styles.content}>
                <FlatList
                    data={props.history}
                    renderItem={(his: any) => <Text>{his.item.query}</Text>}
                    keyExtractor={item => item.id}
                />
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    history: {
        backgroundColor: '#3F3F3F'
    },
    content: {
        justifyContent: 'flex-start',
        padding: 18,
        paddingBottom: 500,
        alignItems: 'center',
    },
    text: {
        color: "#A5BCCB",
    }
});

export default connect(
    (state: any) => {
        return {
            history: selectHistory(state),
            meta: selectHistoryMeta(state),
            account: selectAccount(state),
        }
    }, { getUserSearchHistory, deleteSearchHistory }
)(History);
