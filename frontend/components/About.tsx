import React, { useState } from 'react';
import { StyleSheet, Button, View, Dimensions, Text, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';

const About = () => {
    return (
        <View style={styles.about}>
            <Text>Dette er About-page.</Text>

            <TouchableOpacity onPress={Actions.home}>
                <Text style={styles.bottomText}>Fortsett</Text>
            </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
    about: {
        flex: 1,
        backgroundColor: '#003f5c',
        alignItems: 'center',
        justifyContent: 'center',
    },
    bottomText: {
        color: "#A5BCCB",
        fontSize: 15,
        margin: 30,
    },
});

export default About;