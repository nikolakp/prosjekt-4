import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from "react-redux";
import { selectAccount } from "../store/account/selectors";
import { addPerson, updatePerson } from "../store/person/actions";
import { selectPerson } from "../store/person/selectors";
import Header from "./Header";


const PersonForm = (props: any) => {
    useEffect(() => {
        !props.account && Actions.login()
    }, []);

    if (!props.account) {
        return (
            <View style={styles.form}>
                <Header />
                <View style={styles.content}>
                    <Text style={styles.text}>Logg inn for å endre personer</Text>
                </View>
            </View>
        )
    };

    return (
        <View style={styles.form}>
            <Header />
            <View style={styles.content}>
                <Text style={styles.text}>Legg til person</Text>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    form: {
        backgroundColor: '#3F3F3F'
    },
    content: {
        justifyContent: 'flex-start',
        alignItems: 'center',
        padding: 18,
        paddingBottom: 500,
    },
    text: {
        color: "#A5BCCB",
    }
});

export default connect((state: any, ownProps: any) => {
    return { account: selectAccount(state), person: selectPerson(state, ownProps.personid) };
},
    { addPerson, updatePerson })(PersonForm);
