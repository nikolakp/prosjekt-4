import React, { useState } from 'react';
import { StyleSheet, Button, View, Dimensions, Text, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Alert } from "react-native";

const createOneButtonAlert = (title: string, text: string) => {
  Alert.alert(
      title,
      text,
        {
          style: "dark"
        }
      ,
      { cancelable: true }
    );
}

export default createOneButtonAlert;