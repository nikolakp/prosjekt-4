import { faBars, faBackspace } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import React from 'react';
import { Dimensions, StyleSheet, TouchableOpacity, View, Text } from 'react-native';
import { Avatar } from 'react-native-elements';
import { Actions } from 'react-native-router-flux';
import { connect } from "react-redux";
import { selectAccount, selectAccountMeta } from "../store/account/selectors";
import { selectSearchWord } from "../store/search/selectors";


const Header = (props: any) => {
    const canReset = Actions.currentScene === 'home' && props.searchWord;

    const reset = () => (
        <View style={styles.reset}>
            <FontAwesomeIcon icon={faBackspace} size={30} color="#A5BCCB" />
            <Text style={styles.resetText}>
                NULLSTILL
            </Text>
        </View>
    );

    return (
        <View style={styles.header}>
            <TouchableOpacity style={styles.bars} onPress={Actions.menu}>
                <FontAwesomeIcon icon={faBars} size={40} color="#66D3FA" />
            </TouchableOpacity>
            <Text style={styles.logo}>
                {canReset ? reset() : 'FengselDB'}
            </Text>
            <Avatar rounded size="medium" title={props.account ? props.account.username.slice(0, 2).toUpperCase() : null}
                titleStyle={{ color: '#003f5c' }} icon={{ name: 'face', color: '#003f5c' }}
                containerStyle={{ backgroundColor: '#66D3FA' }} onPress={Actions.login} />
        </View>
    );
}

const styles = StyleSheet.create({
    header: {
        backgroundColor: '#003f5c',
        padding: 20,
        paddingTop: 50,
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: Dimensions.get('window').width,
    },
    bars: {
        marginTop: 'auto',
        marginBottom: 'auto',
    },
    logo: {
        fontWeight: "bold",
        fontSize: 28,
        color: "#66D3FA",
        marginTop: 'auto'
    },
    resetText: {
        fontWeight: "bold",
        fontSize: 22,
        color: "#A5BCCB",
        marginLeft: 10,
        marginTop: 'auto'
    },
    reset: {
        flexDirection: 'row',
    }
});

export default connect((state: any) => {
    return {
        account: selectAccount(state),
        meta: selectAccountMeta(state),
        searchWord: selectSearchWord(state)
    }
}, {})(Header);