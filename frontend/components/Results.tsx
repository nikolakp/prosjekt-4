import moment from 'moment';
import React, { useState } from 'react';
import { Dimensions, FlatList, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { Avatar } from 'react-native-elements';
import { connect } from "react-redux";
import {
    selectSearchResultsForSlugPage,
    selectSearchResultsForSlugPageMeta,
    selectPage
} from "../store/search/selectors";
import PersonPopup from "./PersonPopup";

import { setPage } from "../store/search/actions";

const Results = (props: any) => {
    const [popupId, setPopupId] = useState('');

    const nextPage = () => {
        if (props.meta.loading) return;
        if (props.searchResults.length >= 20 &&
            !(props.searchResults.length % 20)) {
            props.setPage(props.page + 1)
        };
    };

    return (
        <View style={styles.container}>
            <PersonPopup personId={popupId} onClose={() => setPopupId('')} />
            <FlatList
                data={props.searchResults}
                renderItem={(props: any) =>
                    <TouchableOpacity onPress={() => { setPopupId(props.item.id) }}>
                        <Card {...props.item} />
                    </TouchableOpacity>}
                keyExtractor={item => item.id}
                maintainVisibleContentPosition={{
                    minIndexForVisible: 0,
                }}
                initialNumToRender={100}
                onEndReachedThreshold={0.5}
                onEndReached={nextPage}
            />
        </View>
    );
};

const Card = (props: any) => {
    const { name, birthdate, location, sentence, picture } = props;

    return (
        <View style={styles.border}>
            <View style={styles.card}>
                <View style={styles.header}>
                    <View style={styles.image}>
                        <Avatar rounded
                            size="medium"
                            source={{
                                uri: picture,
                            }} />
                    </View>
                    <View>
                        <Text style={styles.name}>
                            {name.first} {name.last}
                        </Text>
                        <Text style={styles.info}>
                            {moment(birthdate).format('DD.MM.YYYY')},{' '}
                            {location.city}
                        </Text>
                        <Text style={styles.info}>
                            Fengslet i {sentence.years} år
                        </Text>
                    </View>
                </View>
            </View>
        </View>
    )
};

const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        paddingBottom: 570,
        backgroundColor: '#3F3F3F'

    },
    border: {
        borderBottomWidth: 1,
        borderColor: '#66D3FA',
    },
    card: {
        backgroundColor: '#002B40',
        borderRadius: 3,
        padding: 15,
        paddingTop: 18,
        paddingBottom: 18,
    },
    header: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    name: {
        fontWeight: "bold",
        color: '#66D3FA'
    },
    info: {
        color: '#D5F3FE'
    },
    image: {
        marginRight: 20
    }
});

export default connect(
    (state: any) => {
        const searchResults = selectSearchResultsForSlugPage(state);
        const meta = selectSearchResultsForSlugPageMeta(state);
        const page = selectPage(state)
        return { searchResults, meta, page };
    }, { setPage })(Results);
