import React, { useEffect, useState } from 'react';
import { Dimensions, StyleSheet, View, Text, Button, TouchableHighlight } from 'react-native';
import { SearchBar } from 'react-native-elements';
import { connect } from "react-redux";
import { searchBySlug, setPage } from "../store/search/actions";
import { selectPage } from "../store/search/selectors";

import { faAngleUp, faMapMarkerAlt, faTimes } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';

const Search = (props: any) => {
    const [searchWord, setSearchWord] = useState('');
    const [filters, setFilters] = useState({
        filters: {
            age: [0, 100]
        },
        sort: {
            key: "name",
            type: "ascending"
        }
    });

    //Filters for the Filter component:
    const [maleActive, setMaleActive] = useState(true); //Male-button
    const [femaleActive, setFemaleActive] = useState(true); //Female-button
    const [showFilter, setShowFilter] = useState(true); //If Filter should be displayed or not!!!
    
    const handleMaleClick = () => { setMaleActive(!maleActive) };
    const handleFemaleClick = () => { setFemaleActive(!femaleActive) };
    const handleFilterClose = () => { console.log("Should close the filter by hiding it. (in Search-component.)");
                                        setShowFilter(false);};
    
    


    const debouncedSearchWord = useDebounce(searchWord, 400);

    useEffect(() => {
        if (props.page) {
            props.searchBySlug(searchWord, filters, props.page);
        }
    }, [props.page]);

    useEffect(() => {
        props.searchBySlug(debouncedSearchWord, filters, props.page)
    }, [debouncedSearchWord]);

    //handles changes to the input field
    const handleChange = (e: string, prev: String) => {
        setSearchWord(e);
        if (!prev.includes(e) || e === '') {
            props.setPage(0);
        };
    };

    const updateFilters = (filters: any) => {
        props.setPage(0);
        setFilters(filters);
    };

    return (
        <View style={styles.search}>
            <SearchBar
                placeholder="Søk her.."
                onChangeText={e => handleChange(e, searchWord)}
                style={styles.bar}
                value={searchWord} />
            <View style={styles.filter}>
                <Text style={styles.colorGray}>
                    Alder
                </Text>
                <Text style={styles.colorGray}>
                    Kjønn
                </Text>
                <Text style={styles.colorGray}>
                    Sortering
                </Text>
            </View>
            {showFilter &&<FilterDisplay male={maleActive} female={femaleActive} onMale={handleMaleClick} onFemale={handleFemaleClick} onFilterClose={handleFilterClose}/>}
        </View>
    );
};

//Component used to show Selection on age | Choose Male or Female | Sort by SortingType Ascending or Descending. 
const FilterDisplay = (props: any) => {
    //Props should be what type of filter that is going to be displayed.
    // 1 Selection on age
    // 2 Choose Male or Female
    // 3 Sort by SortingType Ascending or Descending

    //Takes in props:
    // boolean male
    // boolean female

    let selection = 2 //Just a temporary variable

    
    switch(selection) {
        case 1:
            return (
                <View style={styles.filter}>
                        <Text style={styles.colorGray}>
                            Select Age: Slider here
                        </Text>
                </View>
            );
        case 2:
            return (
                <View style={styles.filterButtons}>
                        <View style={styles.filterButton}>
                        <View style={styles.buttonWidth}>
                            <Button 
                                title="Male"
                                color={props.male ? "#4fa5c4" : "gray"} //Blue for active, gray for not active
                                onPress={() => {
                                    props.onMale(); //Toggles male
                                    console.log("Male button toggled.");}}
                            />
                        </View>

                        <View style={styles.buttonWidth}>
                        <Button 
                            title="Female"
                            color={props.female ? "#4fa5c4" : "gray"} //Blue for active, gray for not active
                            onPress={() => {
                                props.onFemale(); //Toggles female
                                console.log("Female button toggled.");}}
                        />
                        </View>
                        </View>
                        <TouchableHighlight onPress={() => {props.onFilterClose();}}>
                            <FontAwesomeIcon icon={faAngleUp} size={25} color="#404040" />
                        </TouchableHighlight>
                </View>
            );
        case 3:
            return (
                <View style={styles.filter}>
                        <Text style={styles.colorGray}>
                            SORT BY SORTINGTYPE ASCENDING or DESC: 
                        </Text>
                </View>
            );
    }

    
}

//stolen from https://usehooks.com/useDebounce/
const useDebounce = (value: any, delay: any) => {
    const [debouncedValue, setDebouncedValue] = useState(value);

    useEffect(
        () => {
            const handler = setTimeout(() => {
                setDebouncedValue(value);
            }, delay);

            return () => {
                clearTimeout(handler);
            };
        },
        [value, delay]
    );
    return debouncedValue;
};

const styles = StyleSheet.create({
    search: {
        width: Dimensions.get('window').width,
    },
    bar: {
        height: 60
    },
    filter: {
        flexDirection: 'row',
        justifyContent: "space-between",
        padding: 12,
        borderColor: '#66D3FA',
        backgroundColor: '#646464',
    },
    colorGray: {
        color: '#DBDBDB',
    },
    filterButtons: {
        flexDirection: 'row',
        justifyContent: "space-between",
        padding: 12,
        borderColor: '#66D3FA',
        backgroundColor: '#646464',
    },
    filterButton: {
        flex: 1,
        flexDirection: "row",
        justifyContent: "space-evenly",
        // marginHorizontal: "auto",
    },
    buttonWidth: {
        width: 110, //Defines the width of the buttons
    },
    closeButton: {
        backgroundColor: "red",
    }


});

export default connect((state: any) => {
    const page = selectPage(state);
    return { page };
},
    { searchBySlug, setPage })(Search);